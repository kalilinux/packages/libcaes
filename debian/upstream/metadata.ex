# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/libcaes/issues
# Bug-Submit: https://github.com/<user>/libcaes/issues/new
# Changelog: https://github.com/<user>/libcaes/blob/master/CHANGES
# Documentation: https://github.com/<user>/libcaes/wiki
# Repository-Browse: https://github.com/<user>/libcaes
# Repository: https://github.com/<user>/libcaes.git
